<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRentedDevices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rented_devices', function (Blueprint $table) {
            $table->increments('id_rented_device');
            $table->integer('request_id_fk')->unsigned();
            $table->foreign('request_id_fk')->references('id')->on('requests_device');
            $table->enum('rent_status', [
                  'returned',
                  'approved',
                  'overdue'
                  ]);
            $table->date('return_date')->nullable();
            $table->dateTime('returned_at')->nullable();
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rented_devices');
    }
}
