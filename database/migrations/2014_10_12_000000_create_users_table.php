<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password', 60);
            $table->rememberToken();
            $table->timestamps();
            $table->enum('rank', array('admin', 'clan'))->default('clan');
            $table->char('username', 50)->unique();//jel nam uopste treba ovo milose? koristimo imejl za upis
            //$table->char('ime', 100); //vec je u name
            $table->char('lastname', 100)->nullable();
            $table->char('title', 50)->nullable();
            $table->date('date_of_birth')->nullable();
            $table->char('jmbg',13)->nullable();
            $table->char('pic_path',250)->nullable();
            $table->char('phone',50)->nullable();
            $table->char('city',50)->nullable();
            $table->char('street',250)->nullable();
            $table->char('street_num',10)->nullable();
            $table->char('bank',150)->nullable();
            $table->char('b_account',50)->nullable();
            $table->char('doc_path1',250)->nullable();
            $table->char('doc_path2',250)->nullable();
            $table->char('doc_path3',250)->nullable();
            $table->enum('user_status', array('aktivan', 'neaktivan'))->default('neaktivan');
            //ovo treba da bude posebna tabela 
            $table->char('mac',32)->nullable();//neznam zasta sluzi
            $table->tinyInteger('status_mac')->nullable();//neznam zasta sluzi
            $table->integer('prijava')->nullable();//neznam zasta sluzi
            $table->integer('odjava')->nullable();//neznam zasta sluzi
            $table->tinyInteger('mac_synced')->nullable();//neznam zasta sluzi
            $table->tinyInteger('stat_mac_sync')->nullable();//neznam zasta sluzi
            
            
            /*srpski
            $table->tinyInteger('tip');
            $table->char('username', 50);
            $table->char('ime', 100);
            $table->char('prezime', 100);
            $table->char('zvanje', 50);
            $table->date('dat_rodj');
            $table->char('jmbg',13);
            $table->char('pic_path',250);
            $table->char('telefon',50);
            $table->char('adresa_grad',50);
            $table->char('adresa_ulica',250);
            $table->char('adresa_broj',10);
            $table->char('banka',150);
            $table->char('br_rac',50);
            $table->char('doc_path1',250);
            $table->char('doc_path2',250);
            $table->char('doc_path3',250);
            $table->tinyInteger('status_clana');
            $table->char('MAC',32);
            $table->tinyInteger('status_mac');
            $table->integer('prijava');
            $table->integer('odjava');
            $table->tinyInteger('mac_synced');
            $table->tinyInteger('stat_mac_sync');*/
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
