<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesDevices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories_devices', function (Blueprint $table) {
           $table->increments('id_category');
            $table->char('name',50);
           
        });
        
         Schema::create('device_to_category', function (Blueprint $table) {
            $table->integer('device_id');
            $table->integer('categories_device_id');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('categories_devices');
        Schema::drop('device_to_category');
    }
}
