<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAutorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('autors', function (Blueprint $table) {
            $table->increments('id_autor');
            //$table->foreign('id_book_foregin')->references('id_book')->on('books');
            $table->char('title',10)->nullable();
            $table->char('a_name',50);
            $table->char('a_last_name',100);
            //$table->timestamps();
        });
        
         Schema::create('book_to_autor', function (Blueprint $table) {
            $table->integer('book_id');
            $table->integer('autor_id');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('autors');
        Schema::drop('book_to_autor');
    }
}
