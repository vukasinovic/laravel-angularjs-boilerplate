<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTutorialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tutorials', function (Blueprint $table) {
            $table->increments('id');
            $table->char('name', 50);
            $table->char('category', 50);
            $table->dateTime('entry_date');
            $table->char('link', 50);
            $table->tinyInteger('published')->default(0);
            $table->integer('user_id')->unsigned();
            //$table->foreign('user_id')->references('id')->on('user');
            
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tutorials');
    }
}
