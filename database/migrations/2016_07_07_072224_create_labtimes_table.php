<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLabtimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('labtimes', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->dateTime('time');
            $table->integer('user_id')->unsigned();
            //$table->foreign('user_id')->references('id')->on('user');
        });
        Schema::create('sync_rpi', function (Blueprint $table) {
            $table->increments('id');           
            $table->integer('user_id')->unsigned();
            $table->dateTime('time');
            $table->char('type', 50);
            $table->char('query', 50);
            //$table->foreign('user_id')->references('id')->on('user');
        });
        Schema::create('unauththorized_access', function (Blueprint $table) {
            $table->increments('id');
            $table->char('mac',32);
            $table->integer('user_id')->unsigned()->nullable();
            $table->dateTime('time');
            $table->char('username', 50)->nullable();
            $table->char('description', 50)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('labtimes');
        Schema::drop('sync_rpi');
        Schema::drop('unauththorized_access');
    }
}
