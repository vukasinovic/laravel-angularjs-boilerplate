<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->increments('id_book');
            $table->string('name');
            $table->char('language', 100);
            $table->smallInteger('year_of_publication');
            $table->char('isbn', 100);
            $table->text('description');
            $table->char('edition', 100);
            $table->char('purchase', 100);
            $table->decimal('price', 10, 2);
            $table->char('binding', 50);
            //$table->char('category', 50);//$table->integer('category_book_fk')->unsigned();
            //$table->foreign('category_fk')->references('id_category')->on('categories_books');
            $table->enum('book_status', array('rented', 'free'))->default('free');
            $table->tinyInteger('electronic_book');
            $table->char('book_src', 250);
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('books');
    }
}
