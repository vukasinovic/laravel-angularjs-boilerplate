<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRentedBooks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('rented_books', function (Blueprint $table) {
            $table->increments('id_rented_book');
            $table->integer('request_id_fk')->unsigned()->unique();
            $table->foreign('request_id_fk')->references('id')->on('requests_books');
            $table->enum('rent_status', [
                  'returned',
                  'approved',
                  'overdue'
                  ]);
            $table->date('return_date')->nullable();
            $table->dateTime('returned_at')->nullable();
           
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('rented_books');
    }
}
