<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesBooks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('categories_books', function (Blueprint $table) {
            $table->increments('id_category');
            $table->char('name',50);
            
        });
        
         Schema::create('book_to_category', function (Blueprint $table) {
            $table->integer('book_id');
            $table->integer('categories_books_id');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('categories_books');
         Schema::drop('book_to_category');
    }
}
