<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('devices', function (Blueprint $table) {
            $table->increments('id_device');
            $table->char('type',50);
            //$table->char('category',50);
            $table->char('description',250);
            $table->char('mac',20)->nullable();
            $table->char('serial_num',30)->nullable();
            $table->char('os_vresion',20)->nullable();
            $table->char('dimensions',40)->nullable();
            $table->char('extra_equipment',250)->nullable();
            $table->enum('device_status',  array('rented', 'free'))->default('free');
            $table->char('index',12)->nullable();
            $table->timestamps();
            //$table->primary('id_device');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('devices');
    }
}
