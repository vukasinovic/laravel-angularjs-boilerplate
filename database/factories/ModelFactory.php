<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
        'rank'=>"clan",
        'username'=>$faker->text($maxNbChars = 50),
        'title'=>$faker->text($maxNbChars = 50),
        'date_of_birth'=>$faker->dateTimeThisCentury,
        'jmbg'=>$faker->text($maxNbChars = 13),
        'pic_path'=>$faker->text($maxNbChars = 250),
        'phone'=>$faker->phoneNumber,//text($maxNbChars = 50),
        'city'=>$faker->city,   //text($maxNbChars = 50),
        'street'=>$faker->streetName,//text($maxNbChars = 250),
        'street_num'=>$faker->buildingNumber,//text($maxNbChars = 10),
        'bank'=>$faker->text($maxNbChars = 150),
        'b_account'=>$faker->numberBetween(5000, 9000),
        'doc_path1'=>$faker->text($maxNbChars = 250),
        'doc_path2'=>$faker->text($maxNbChars = 250),
        'doc_path3'=>$faker->text($maxNbChars = 250),
        'user_status'=>"neaktivan"
           
        
    ];
});
//https://github.com/fzaninotto/Faker
//instrukcije za faker biblioteku
//za knjigu fabrika
//$factory->define(App\Knjige::class, function (Faker\Generator $faker) {
//    return [
//            'naslov'=>$faker->name,
//            'language'=>$faker->text($maxNbChars = 100),
//            'godina_izdanja'=>$faker->dateTimeThisCentury,
//            'isbn'=>$faker->isbn13,
//            'opis'=>$faker->sentences($nb = 3, $asText = true),
//            'izdanje'=>$faker->numberBetween(0,6),
//            'nabavka'=>$faker->text($maxNbChars = 60),
//            'cena'=>$faker->numberBetween(100,200),
//            'povez'=>$faker->text($maxNbChars = 60),
//            'kategorija'=>$faker->text($maxNbChars = 60),
//            'status_knjige'=>$faker->boolean($chanceOfGettingTrue = 50),
//            'elektronska' =>$faker->boolean($chanceOfGettingTrue = 50),
//            'knjiga_src' =>str_random(10),
//        
//    ];
//});
$factory->define(App\Book::class, function (Faker\Generator $faker) {
    return [
            'name'=>$faker->name,
            'language'=>$faker->text($maxNbChars = 100),
            'year_of_publication'=>"1999",
            'isbn'=>$faker->isbn13,
            'description'=>$faker->sentences($nb = 3, $asText = true),
            'edition'=>$faker->numberBetween(0,6),
            'purchase'=>$faker->text($maxNbChars = 60),
            'price'=>$faker->numberBetween(100,200),
            'binding'=>$faker->text($maxNbChars = 20),
            
            'book_status'=>"free",
            'electronic_book' =>$faker->boolean($chanceOfGettingTrue = 50),
            'book_src' =>str_random(10),
        
    ];
});
$factory->define(App\Device::class, function (Faker\Generator $faker) {
    return [
            'type'=>$faker->text($maxNbChars = 50),
            'description'=>$faker->text($maxNbChars = 250),
            'mac'=>$faker->text($maxNbChars = 20),
            'serial_num'=>$faker->text($maxNbChars = 30),
            'os_vresion'=>$faker->text($maxNbChars = 20),
            'dimensions'=>$faker->text($maxNbChars = 40),
            'extra_equipment'=>$faker->text($maxNbChars = 250),
            'device_status'=>"free",
            'index'=>$faker->text($maxNbChars = 12),

    ];
});

