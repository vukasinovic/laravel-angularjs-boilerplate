<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //$this->call('TabelaKnjigeSeeder');
        DB::table('users')->insert([
            'name' => 'sara',
            'lastname' => 'mikic',
            'email' => 'sara@gmail.com',
            'password' => bcrypt('sarasara'),
            'username'=>'username1',
            'rank'=>'admin',
            'mac'=>'6ED5635522BD',
        ]);
        DB::table('users')->insert([
            'name' => 'milos',
            'email' => 'milos@gmail.com',
            'password' => bcrypt('sarasara'),
            'username'=>'username2',
            'rank'=>'admin',
        ]);
        $this->call('BooksTableSeeder');
        $this->call('UsersTableSeeder');
        $this->call('DeviceTableSeeder');
        $this->call('ConstTableSeeder');
       
             
      
        $data = array(
             array('user_id' => 1, 'permission'=>1),
             array('user_id' => 1, 'permission'=>2),
             array('user_id' => 1, 'permission'=>3),
             array('user_id' => 1, 'permission'=>4),
             array('user_id' => 1, 'permission'=>5),
             array('user_id' => 1, 'permission'=>7),
             array('user_id' => 2, 'permission'=>3),
             array('user_id' => 2, 'permission'=>6),
          );
        DB::table('permissions')->insert($data);
        $data = array(
             array('user_id' => 1, 'book_id'=>1, 'request_status'=>'wait'),
             array('user_id' => 1, 'book_id'=>2, 'request_status'=>'wait'),
             array('user_id' => 1, 'book_id'=>3, 'request_status'=>'denied'),
             array('user_id' => 1, 'book_id'=>3, 'request_status'=>'wait'),
          );
        DB::table('requests_books')->insert($data);
        $data = array(
             array('request_id_fk' => 1, 'rent_status'=>'approved', 'return_date'=>'2016-05-17 10:57:33'),
             array('request_id_fk' => 2, 'rent_status'=>'overdue', 'return_date'=>'2016-03-17 10:57:33'),

          );
        DB::table('rented_books')->insert($data);  
        $data = array(
             array('name' => 'android'),
             array('name' => 'C#'),
             array('name' => 'web dizajn'),
             array('name' => 'php'),          
          );
        DB::table('categories_books')->insert($data);  
        $data = array(
             array('categories_books_id' => 1, 'book_id' => 1),
             array('categories_books_id' => 2, 'book_id' => 1),
             array('categories_books_id' => 2, 'book_id' => 4),
             array('categories_books_id' => 2, 'book_id' => 3),
             array('categories_books_id' => 3, 'book_id' => 5),            
         );
        DB::table('book_to_category')->insert($data);  
        $data = array(
            array('a_last_name' => "Miloš", 'a_name' =>"Marinković",'title'=>''),
            array('a_last_name' => "Sanja", 'a_name' =>"Rotinger",'title'=>'prof.'),
            array('a_last_name' => "Ognjen", 'a_name' =>"Black",'title'=>'mr.prof.'),
            array('a_last_name' => "Ajra", 'a_name' =>"Krstić",'title'=>'dr.'),
            array('a_last_name' => "Kola", 'a_name' =>"Mančić",'title'=>'ing.'),
         );
        
        
        DB::table('autors')->insert($data);
        $data = array(
             array('autor_id' => 1, 'book_id' => 1),
             array('autor_id' => 2, 'book_id' => 1),
             array('autor_id' => 2, 'book_id' => 4),
             array('autor_id' => 2, 'book_id' => 3),
             array('autor_id' => 3, 'book_id' => 5),            
         );
        DB::table('book_to_autor')->insert($data);
    }
    
}
