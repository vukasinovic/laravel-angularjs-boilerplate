<?php

use Illuminate\Database\Seeder;

class ConstTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('constrains')->insert([
            'value_name' => 'deadline',
            'const_value'=>14          
            ]);
        DB::table('constrains')->insert([
            'value_name' => 'admin_mail',
            'const_value'=>'appsteam@vtsnis.edu.rs'          
            ]);
        DB::table('constrains')->insert([
            'value_name' => 'pic_sizeMB',
            'const_value'=>'1'          
            ]);
        DB::table('constrains')->insert([
            'value_name' => 'doc_sizeMB',
            'const_value'=>'1'          
            ]);
        DB::table('constrains')->insert([
            'value_name' => 'mail0',
            'const_value'=>
            'Administrator biblioteke VTŠ AppsTim'          
            ]);
        DB::table('constrains')->insert([
            'value_name' => 'mail1',
            'const_value'=>
            'Imate novi zahtev za izdavanje knjige.
Pogledajte zahteve na =>link ubaci'          
            ]);
        DB::table('constrains')->insert([
            'value_name' => 'mail2',
            'const_value'=>
            'Imate novi zahtev za izdavanje uređaja.
Pogledajte zahteve na =>link ubaci'          
            ]);
        DB::table('constrains')->insert([
            'value_name' => 'mail3',
            'const_value'=>
            'Knjiga je izdata članu koji je zahtev poslao pre vas. Još uvek ste na listi čekakanja.'          
            ]);
         DB::table('constrains')->insert([
            'value_name' => 'mail4',
            'const_value'=>
            'Uređaj je izdat članu koji je zahtev poslao pre vas. Još uvek ste na listi čekakanja.'          
            ]);
        DB::table('constrains')->insert([
            'value_name' => 'mail5',
            'const_value'=>
            'Odobreno vam je iznajmljivanje knjige.'          
            ]);
        DB::table('constrains')->insert([
            'value_name' => 'mail6',
            'const_value'=>
            'Odobreno vam je iznajmljivanje uređaja.'          
            ]);
         DB::table('constrains')->insert([
            'value_name' => 'mail7',
            'const_value'=>
            'Kasnite sa vraćanjem ove knjige.'          
            ]);
         DB::table('constrains')->insert([
            'value_name' => 'mail8',
            'const_value'=>
            'Kasnite sa vraćanjem ovog uređaja.'          
            ]);
    }
}

