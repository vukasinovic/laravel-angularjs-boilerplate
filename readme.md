## Laravel 5 and Angular2 Boilerplate

Documentation for the framework can be found on the [Laravel website](http://laravel.com/docs).

This is boilerplate app example of integration Laravel(5.3) and AngularJS(2.0.2)

## Contributing

### Aleksandar Vukasinovic

## Instruction and installation

1) `npm install`

2) `npm run dev` and `php artisan serve`

## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).