<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

/* Return blade view */

Route::get('/', function () {
    return view('index');
});


//kad su ovde ispred je public/...

//  https://github.com/tymondesigns/jwt-auth/wiki/Creating-Tokens
//  https://www.toptal.com/web/cookie-free-authentication-with-json-web-tokens-an-example-in-laravel-and-angularjs
Route::post('/auth','AutenticateControler@authenticate');
/*Route::get('/bazareform', function () {
    $exitCode = Artisan::call('migrate:refresh'
           // , [ '--seed' => 'default' ]
            ); 
    echo "done";
});*/

//'prefix' => 'api',
Route::group(['middleware' => ['jwt.auth','iptoken']], function()
{
    Route::get('/self', 'UserController@selfUser');//done
    Route::post('/passchange', 'UserController@passwordChanger');//done
    
    Route::get('/sync', 'UserController@sync');//droped
    //lab in time
    Route::post('/labtime', 'labTimeControler@addLabTime');//droped
    Route::post('/unauth', 'labTimeControler@addUnauththorizedAccess');//droped
    Route::post('/rpisync', 'labTimeControler@addSyncRpi');//droped
    
    
    //Route::group(['middleware' => ['permisioncheck:books']],function(){
        Route::get('/books', 'BookController@allBooks');//done
    //});// ovako za svaku rutu napravi permisiju samo zameni books sa imenom permisije
    
    //Route::group(['middleware' => ['permisioncheck:books_super']],function(){
        Route::get('/books/requests', 'Requests_bookControler@requestsBooksAll');//gives all requests //done
        Route::post('/books/requests', 'Requests_bookControler@requestBook');//adds request//done
        Route::patch('/books/requests', 'Requests_bookControler@finalizeRequestBook');//done
        Route::get('/books/requests/waitlist', 'Requests_bookControler@waitList');//spisak za odobrenje//done
        
        Route::get('/books/rent/approved', 'Rented_booksControler@approvedList');//lista za vracanje//done
        Route::get('/books/rent', 'Rented_booksControler@allRentedBooks');//gives all requests//history//done
        Route::patch('/books/rent', 'Rented_booksControler@updateRentedtBook');//PATCH//done
        
        //BOOK-CATEGORY
        Route::get('/books/cat', 'Categories_booksController@allCategoriesBook');//done
        Route::post('/books/cat', 'Categories_booksController@addCategoryBook');//POST//url encode dont forget
        Route::patch('/books/cat', 'Categories_booksController@modifyCategoryBook');//done
        Route::delete('/books/cat/{id_cat}', 'Categories_booksController@deleteCategoryBook');//done
//doing
        //Autori
        Route::get('/books/autors', 'AutorsControler@allAutors');//done
        Route::delete('/books/autors/{id_autor}', 'AutorsControler@deleteAutor');//done
        Route::post('/books/autors', 'AutorsControler@addAutor');//done
        Route::patch('/books/autors', 'AutorsControler@modifyAutor');//done
        
        Route::get('/books/{book}', 'BookController@oneBook');//done
        Route::post('/books', 'BookController@addBook');//done
        Route::patch('/books/{book}', 'BookController@modifyBook');//PATCH//done 
        Route::delete('/books/{book}', 'BookController@deleteBook');//done 
        // NEMA DONJIH CRTICA       
               
    //});
    //Route::group(['middleware' => ['permisioncheck:users']],function(){ 
        Route::get('/users', 'UserController@allUsers');//done
    //});
    //Route::group(['middleware' => ['permisioncheck:users_super']],function(){
        Route::post('/users', 'UserController@addUser');//done
        Route::get('/users/{user}', 'UserController@oneUser');//done
        Route::patch('/users/{user}', 'UserController@modifyUser');//done
        Route::delete('/users/{user}', 'UserController@deleteUser');//done
    //});
    //Route::group(['middleware' => ['permisioncheck:devices']],function(){
        Route::get('/devices', 'DeviceController@allDevices');//done
    
    //});
    
    //Route::group(['middleware' => ['permisioncheck:devices_super']],function(){
        
        Route::get('/devices/cat', 'Categories_devicesController@allCategoriesDevice');//done
        Route::post('/devices/cat', 'Categories_devicesController@addCategoryDevice');//done
        Route::patch('/devices/cat', 'Categories_devicesController@modifyCategoryDevice');//done
        Route::delete('/devices/cat/{id_cat}', 'Categories_devicesController@deleteCategoryDevice');//done
        
        Route::get('/devices/requests', 'Requests_devicesControler@requestsDevicesAll');//gives all requests//done
        Route::get('/devices/requests/waitlist', 'Requests_devicesControler@waitList');//spisak za odobrenje//done
        Route::post('/devices/requests', 'Requests_devicesControler@requestDevice');//adds request//done
        Route::patch('/devices/requests', 'Requests_devicesControler@finalizeRequestDevice');//done
        
        Route::get('/devices/rent', 'Rented_devicesControler@allRentedDevices');//gives all requests//history//done
        Route::get('/devices/rent/appr_list', 'Rented_devicesControler@approvedList');//lista za vracanje//done
        Route::patch('/devices/rent', 'Rented_devicesControler@updateRentedtDevice');//done
        
        Route::get('/devices/{device}', 'DeviceController@oneDevice');//done
        Route::post('/devices', 'DeviceController@addDevice');//done
        Route::patch('/devices', 'DeviceController@modifyDevice');//done
        Route::delete('/devices/{device}', 'DeviceController@deleteDevice');//done
        
        
    //});
    
    //Route::group(['middleware' => ['rankcheck:admin']],function(){
        Route::get('/permissions', 'PermissionController@allPermisions');//done
        Route::get('/permissions/{userId}', 'PermissionController@getPermisions');//done
        Route::post('/permissions', 'PermissionController@setPermisions');//done
        
        Route::patch('/const', 'ConstraintsControler@edit');//done
        Route::get('/const', 'ConstraintsControler@all');//done
        Route::get('/const/{value}', 'ConstraintsControler@specific');//done
    //});
});
