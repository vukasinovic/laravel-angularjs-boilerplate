import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { routes } from './app.routing';
import { AppComponent } from './app.component';

import { LoginComponent } from "./components/login/login.component";
import { HttpAppService } from "./services/http.service";

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        RouterModule.forRoot(routes, {
            useHash: true
        })
    ],
    declarations: [
        AppComponent,
        LoginComponent
    ],
    providers: [
        HttpAppService
    ],
    bootstrap:[
        AppComponent
    ]
})
export class AppModule {}
