import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/delay';
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class HttpAppService {
    private http: Http;

    constructor (http: Http) {
        this.http = http;
    }

    /**
     * @param requestUrl
     * @returns {Observable<{}>}
     */

    public getData (requestUrl: string) {
        return this.http
            .get(requestUrl)
            // ...and calling .json() on the response to return data
            .map((res:Response) => res.json())
            //...errors if any
            .catch((error:any) => Observable.throw(error.json().error || 'Server error'));

    }

}
