import { Component } from '@angular/core';
import {HttpAppService} from "../../services/http.service";


@Component({
    selector: 'login',
    template: require('./login.template.html'),
    styles: [require('./login.template.css')],
    providers: [HttpAppService]
})
export class LoginComponent {
    response = [];

    constructor (private httpAppService: HttpAppService) {
        this.httpAppService.getData('http://date.jsontest.com/')
            .subscribe(
                data => this.response = data,
                error => alert(error),
                () => console.log('finished')
            );
    }

}