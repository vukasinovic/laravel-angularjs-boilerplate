<!DOCTYPE html>
<html>
<head>
    <title>Apps Team Portal</title>
    <base href="/"/>
</head>
<body>
    <div class="container">
        <div class="content">
            <app>
                <div class="loading-placeholder-wrapper">
                    <div class="loading-placeholder-inner-wrapper">
                        <span class="loading-placeholder">Loading...</span>
                    </div>
                </div>
            </app>
            <script src="{{ elixir('vendor.js', '') }}"></script>
            <script src="{{ elixir('main.js', '') }}"></script>
        </div>
    </div>
</body>
</html>
