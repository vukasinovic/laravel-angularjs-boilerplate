@extends('layouts.app')

@section('content')
 <div class="panel-body">
        <!-- Display Validation Errors -->
        @include('common.errors')
        <!-- New Book Form -->
        <form action="{{ url('book') }}" method="POST" class="form-horizontal">
            {!! csrf_field() !!}

            <!-- Book Name -->
            <div class="form-group">
                <label for="task" class="col-sm-3 control-label">Book</label>

                <div class="col-sm-6">
                    Name:<input type="text" name="name" id="book-name" class="form-control">
                    Lang:<input type="text" name="language" id="book-language" class="form-control">
                    Year:<input type="number" name="year_of_publication" id="book-year_of_publication" class="form-control">
                </div>
            </div>

            <!-- Add Task Button -->
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="submit" class="btn btn-default">
                        <i class="fa fa-plus"></i> Add Book
                    </button>
                </div>
            </div>
        </form>
    </div>

    <!-- Current Books -->
     @if (count($books) > 0)
        <div class="panel panel-default">
            <div class="panel-heading">
                Current Books
            </div>

            <div class="panel-body">
                <table class="table table-striped task-table">

                    <!-- Table Headings -->
                    <thead>
                        <th>Book Name</th>
                        <th>Book Lang</th>
                        <th>Book Year</th>
                        <th>&nbsp;</th>
                    </thead>

                    <!-- Table Body -->
                    <tbody>
                        @foreach ($books as $book)
                            <tr>
                               
                                <td class="table-text">
                                    <div>{{ $book->name }}</div>
                                </td>
                                 <td class="table-text">
                                    <div>{{ $book->language }}</div>
                                </td> 
                                <td class="table-text">
                                    <div>{{ $book->year_of_publication }}</div>
                                </td>
                                <td>
                                    <!-- TODO: Delete Button -->
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endif
@endsection