<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    //protected $table='devices';
    protected $primaryKey='id_device';
    //protected $dateFormat='U';
    protected $fillable = [
        'type',
        'category',
        'description',
        'mac',
        'serial_num',
        'os_vresion',
        'dimensions',
        'extra_equipment',
        'device_status',
        'index',
    ];
    
    public function categories_device(){
        return $this->belongsToMany('App\Categories_devices','device_to_category');
    }
    
}
