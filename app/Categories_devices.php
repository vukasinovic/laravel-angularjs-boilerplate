<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories_devices extends Model
{
    //
    //protected $table='books';
    public $timestamps = false;
    protected $primaryKey='id_category';
    //protected $dateFormat='U';
    protected $fillable = [
        'name',
       
    ];
    
    public function device(){
        return $this->belongsToMany('App\Device','device_to_category');
    }
}

