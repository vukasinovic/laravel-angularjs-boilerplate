<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Http\Response;
class Permissions extends Model
{
     public $timestamps = false;
  public function user()
    {
        return $this->belongsTo('App\User','id','fk_id_user');
    }
    public function getPermisionName() {
        return $this['attributes']['permission'];
    } 
    public static function permisionExists($id,$name) {
        $per= DB::table('permissions')->where('user_id', $id)->where('permission', $name)->get();
        //var_dump($per);
        if (!empty($per)){
            return true;
        }else{
            return false;
        }
       
    }
}
