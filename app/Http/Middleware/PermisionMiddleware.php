<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use App\Permissions;
use Illuminate\Http\Response;
class PermisionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$permision)//,$permision
    {
        $token = JWTAuth::getToken();
        $token2=JWTAuth::getPayload($token);
        //get id of user
        
        //check if he has $permision
        
        //$a=Permissions::permisionExists($token2['id'],$permision);
        //var_dump($a);
        //if he dosent send error no 
        //permisionExists()
        if(Permissions::permisionExists($token2['id'],$permision))
        {
            return $next($request);        
        }else{
            return (new Response(array('error'=>'no_permission', 'code'=>404), 404)) ->header('Content-Type', 'application/json');
        }
    }
}
