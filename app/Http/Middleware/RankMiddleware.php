<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Illuminate\Http\Response;
class RankMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$nameRank)
    {
        $token = JWTAuth::getToken();
        $token2= JWTAuth::getPayload($token);
        //echo $token2['rank'];
        //echo $nameRank;
        //echo "not_".$nameRank;
        if($token2['rank']==$nameRank){
            return $next($request);
        }else{
            return (new Response(array('error'=>"not_".$nameRank, 'code'=>404), 404)) ->header('Content-Type', 'application/json');
        }
        
        
    }
}
