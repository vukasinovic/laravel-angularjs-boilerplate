<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
class IpCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //$user = JWTAuth::parseToken()->toUser();
    //$user = JWTAuth::parseToken()->authenticate();
    //$token = JWTAuth::getToken();
     //return Response::json(compact('user'));
     ////Milose ovde refresh time
//        $token = JWTAuth::getToken();
//        if($token == null){
//            throw new BadRequestHtttpException('Token not provided');
//        }
//        try{
//            $token = JWTAuth::refresh($token);
//        }catch(TokenInvalidException $e){
//            throw new AccessDeniedHttpException('The token is invalid');
//        }

        try {
            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }
        
        $token = JWTAuth::getToken();
        $token2=JWTAuth::getPayload($token);

        
        if(!($token2['ip']===$_SERVER['REMOTE_ADDR'] || $token2['ip']==="160.99.37.225")) {
            return response()->json(['error' => 'invalid_credentials_ip'], 401);
        }else{
            $user->saveToken($user->getId(),$token2['jti']);
            
            return $next($request);
        }
    }
}
