<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use Validator;
use JWTAuth;
use App\Book;
use DB;
use Tymon\JWTAuth\Exceptions\JWTException;

class BookController extends Controller
{
   
    public function allBooks() {
        $books=  \App\Book::all();       
        //return (new Response($books, 200)) ->header('Content-Type', 'application/json');
       foreach ($books as $book) {
            $book->categories_book;
            $book->autors;
        }
        if($books===null)
            return (new Response(array('error'=>'books_not_found', 'code'=>404), 404)) ->header('Content-Type', 'application/json');
        else
            return response()->json($books);//radi
    }
    public function oneBook($id) {   
         if( ! is_numeric($id))//proveri id
          { return (new Response(array('error'=>'id_nije_broj'), 404)) ->header('Content-Type', 'application/json');}
          
        $book=  \App\Book::find($id);        
        $cat=$book->categories_book;
        $aut=$book->autors;
//        $categories=array();
//        foreach ($book->categories_book as $out) {
//            //var_dump($out->name);
//            $categories[]=$out->name;
//        }
//        $autors=array();
//        foreach ($book->autors as $out) {
//            //var_dump($out->name);
//            $autors[]=$out->a_name.' '.$out->a_last_name.','.$out->title;
//        }
//        //var_dump($categories);
        
        if($book===null)
            return (new Response(array('error'=>'book_not_found', 'code'=>404), 404)) ->header('Content-Type', 'application/json');
        else
            return response()->json($book);//radi
    }
   
    public function addBook(Request $request){
        
        $rules = array(
        'name' => 'required',         
        );
        $messages = [
            'required' => ':attribute polje je obavezno.',            
        ];
        $validator = Validator::make($request->all(), $rules, $messages);//$messages
        if ($validator->fails()) {        
            $r_messages = $validator->messages();
            return (new Response(array('error'=>$r_messages, 'code'=>404), 404)) ->header('Content-Type', 'application/json');
        } else {
            $book = new \App\Book;
            $book->name     =  $request->name;
            $book->language =  $request->language;
            $book->year_of_publication =  $request->year_of_publication;
            $book->isbn     =  $request->isbn;
            $book->description =  $request->description;
            $book->edition  =  $request->edition;
            $book->purchase =  $request->purchase;
            $book->price    =  $request->price;
            $book->binding  =  $request->binding;
            //$book->book_status = $book->book_status;
            $book->electronic_book =  $request->electronic_book;
            $book->book_src =  $request->book_src;
           
            
            try{                    
                    if($book->save()){
                        foreach ($request->categories_book as $cat)
                        DB::table('book_to_category')->insert([
                            'book_id' => $book->id_book,
                            'categories_books_id'=>$cat
                            ]);
                        foreach ($request->autors as $authorID)
                        DB::table('book_to_autor')->insert([
                            'book_id' => $book->id_book,
                            'autor_id'=>$authorID
                            ]);
                        return (new Response(array('succes'=>'book_added'), 200)) ->header('Content-Type', 'application/json');
                    }
                              
            }catch (\Illuminate\Database\QueryException $e){
                return (new Response(array('error'=>$e), 404)) ->header('Content-Type', 'application/json');
            }
        }
   }
    
    public function modifyBook(Request $request,$id){
         if( ! is_numeric($id))//proveri id
          { return (new Response(array('error'=>'id_nije_broj'), 404)) ->header('Content-Type', 'application/json');}
        
        $book=\App\Book::find($id);
        if ($book === null) {//ako nepostoji book
          return (new Response(array('error'=>"book_nepostoji"), 404)) ->header('Content-Type', 'application/json');
        }else{
            $rules = array(
            'name' => 'required', 
            
            );
            $messages = [
                'required' => ':attribute polje je obavezno.',            
            ];
            $validator = Validator::make($request->all(), $rules, $messages);//$messages
            if ($validator->fails()) {        
                $r_messages = $validator->messages();
                return (new Response(array('error'=>$r_messages, 'code'=>404), 404)) ->header('Content-Type', 'application/json');
            } else {
                $book->name     =  $request->name;
                $book->language =  $request->language;
                $book->year_of_publication =  $request->year_of_publication;
                $book->isbn     =  $request->isbn;
                $book->description =  $request->description;
                $book->edition  =  $request->edition;
                $book->purchase =  $request->purchase;
                $book->price    =  $request->price;
                $book->binding  =  $request->binding;
                //$book->book_status =  $request->book_status;
                $book->electronic_book =  $request->electronic_book;
                $book->book_src =  $request->book_src;

                try{
                    $book->save();
                    
                     
                        //kategorije
                            DB::table('book_to_category')->
                                 where('book_id' , '=', $book->id_book)->
                                 delete();   //izbrisi prethodne kategorije
                            foreach ($request->categories_book as $cat)
                                {                         
                                         DB::table('book_to_category')->insert([
                                            'book_id' => $book->id_book,
                                            'categories_books_id'=>$cat
                                        ]); 
                                        //echo $cat;
                                }//dodaj nove kategorije
                            //autori  
                            DB::table('book_to_autor')->
                                where('book_id' , '=', $book->id_book)->
                                delete();//delete previus autors
                            foreach ($request->autors as $autorID)
                                {                         
                                         DB::table('book_to_autor')->insert([
                                            'book_id' =>$book->id_book,
                                            'autor_id'=>$autorID
                                        ]); 
                                         // echo $autorID;
                                }    //add new autors

                            return (new Response(array('succes'=>'book_modifyed'), 200)) ->header('Content-Type', 'application/json');

                       
                }catch (\Illuminate\Database\QueryException $e){
                    return (new Response(array('error'=>$e), 404)) ->header('Content-Type', 'application/json');
                }
            }
        }       
    }
    public function deleteBook($id){
        if( ! is_numeric($id))//proveri id
            { return (new Response(array('error'=>'id_nije_broj'), 404)) ->header('Content-Type', 'application/json');}
        $book=\App\Book::find($id); 
        try{
            if($book===null){
                return (new Response(array('error'=>'book_not_found'), 200)) ->header('Content-Type', 'application/json');
            }else{               
                    //delete categories bonds
                    DB::table('book_to_category')->where('book_id', $id)->delete();
                    //delete autors bonds
                    DB::table('book_to_autor')->where('book_id', $id)->delete();
                    //delete book
                    $pass=$book->delete();

                    if($pass){  
                        return (new Response(array('succes'=>'book_deleted'), 200)) ->header('Content-Type', 'application/json');
                    }             
            }        
        } catch (\Illuminate\Database\QueryException $e){
            return (new Response(array('error'=>$e), 404)) ->header('Content-Type', 'application/json');
        } 
    }
    
}
