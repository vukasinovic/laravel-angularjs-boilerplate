<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use  App\Permissions;
use DB;
class PermissionController extends Controller
{
     public function getPermisions($idUser) {
         if( ! is_numeric($idUser))//proveri id
          { return (new Response(array('error'=>'id_nije_broj'), 404)) ->header('Content-Type', 'application/json');}
          
        $user=  \App\User::find($idUser);
        if($user===null)
        {
            return (new Response(array('error'=>'user_not_found', 'code'=>404), 404)) ->header('Content-Type', 'application/json');
        }else{
            $permisions= \App\User::find($idUser)->permissions;
            $jsonpermision=array();
            foreach ($permisions as $permision) {
                $jsonpermision[]=$permision->getPermisionName();
            }
            return (new Response($jsonpermision, 200)) ->header('Content-Type', 'application/json');
            
        }
    }
     public function allPermisions() {
        $permisions= \App\Permissions::all();
        //return $users->toJson();
        if($permisions===null)
            return (new Response(array('error'=>'permissions_not_found', 'code'=>404), 404)) ->header('Content-Type', 'application/json');
        else
            return (new Response($permisions, 200)) ->header('Content-Type', 'application/json');
    }
    
    public function setPermisions(Request $request) {
        
        if( ! is_numeric($request->id))//proveri id
          { return (new Response(array('error'=>'id_nije_broj'), 404)) ->header('Content-Type', 'application/json');}
        $user=  \App\User::find($request->id); 
        $permisions= \App\User::find($request->id)->permissions;
        //var_dump($permisions);
        $esisting_permisions=array();
        foreach ($permisions as $permision) {
                $esisting_permisions[]=$permision->getPermisionName();
            }
        //var_dump($request->permisions); 
        
        //echo 'existing permisions';    
        //var_dump($esisting_permisions);   
        foreach ($request->permisions as $key=>$value) {
          
            $new_permision=$request->permisions[$key];
            //echo  $new_permision;
            if(in_array($new_permision,$esisting_permisions)){
                //echo 'already there</br>';
                $esisting_permisions = array_diff($esisting_permisions, array($new_permision));
            }else{
                //echo 'save new permision</br>';
                $save_permision=new \App\Permissions;
                $save_permision->permission=$new_permision;
                try{
                    $user->permissions()->save($save_permision);
                }  catch (\Illuminate\Database\QueryException $e){
                    return (new Response(array('error'=>$e), 404)) ->header('Content-Type', 'application/json');
                }
            }            
        }
        if($esisting_permisions!=null){
            //echo 'delete these permisions</br>';
            //var_dump($esisting_permisions);
            foreach ($esisting_permisions as $key => $value) {
                //echo "$key = $value\n";
                try{
                    DB::table('permissions')->where('user_id', $request->id)->where('permission',$value)->delete();
                }  catch (\Illuminate\Database\QueryException $e){
                    return (new Response(array('error'=>$e), 404)) ->header('Content-Type', 'application/json');
                }
            }
        }
        return (new Response(array('success'=>'permissions_updated', 'code'=> 200),200)) ->header('Content-Type', 'application/json');
    }
}
