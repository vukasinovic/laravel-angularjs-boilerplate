<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class Categories_devicesController extends Controller
{
    public function addCategoryDevice (Request $request){
        $name=$request->name;
        if($name==null)
        { return (new Response(array('error'=>'name_empty'), 404)) ->header('Content-Type', 'application/json');}    
        $cat=new \App\Categories_devices;
        $cat->name=$name;
        try{
            $cat->save();
            return (new Response(array('succes'=>'category_devices_added'), 200)) ->header('Content-Type', 'application/json');
        } catch (\Illuminate\Database\QueryException $e){
            return (new Response(array('error'=>$e), 404)) ->header('Content-Type', 'application/json');
        } 
    }
    
     public function modifyCategoryDevice (Request $request){
         $idcat=$request->id_cat;
         $nameNew=$request->name;
        if( ! is_numeric($idcat))//proveri id
        { return (new Response(array('error'=>'id_nije_broj'), 404)) ->header('Content-Type', 'application/json');}
         if($nameNew==null)
        { return (new Response(array('error'=>'name_empty'), 404)) ->header('Content-Type', 'application/json');}    
        
        $cat= \App\Categories_devices::find($idcat);
        $cat->name=$nameNew;
        
        try{
            $cat->save();
            return (new Response(array('succes'=>'category_devices_modifyed'), 200)) ->header('Content-Type', 'application/json');
        } catch (\Illuminate\Database\QueryException $e){
            return (new Response(array('error'=>$e), 404)) ->header('Content-Type', 'application/json');
        }
    }
    
    public function deleteCategoryDevice ($idcat){
        if( ! is_numeric($idcat))//proveri id
        { return (new Response(array('error'=>'id_nije_broj'), 404)) ->header('Content-Type', 'application/json');}
         $cat= \App\Categories_devices::find($idcat);
         try{
         $cat->delete();
          return (new Response(array('succes'=>'category_devices_deleted'), 200)) ->header('Content-Type', 'application/json');
        } catch (\Illuminate\Database\QueryException $e){
            return (new Response(array('error'=>$e), 404)) ->header('Content-Type', 'application/json');
        }
    }
    public function allCategoriesDevice (){
        $categories= \App\Categories_devices::all();
        return (new Response($categories, 200)) ->header('Content-Type', 'application/json');
    }
}

