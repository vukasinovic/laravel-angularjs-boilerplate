<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use DB;
use Validator; 
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use File;
use Illuminate\Support\Facades\Hash;
use App\Permissions;
class UserController extends Controller
{
    public function allUsers() {
        $users= \App\User::all();
        //return $users->toJson();
        if($users===null)
            return (new Response(array('error'=>'users_not_found', 'code'=>404), 404)) ->header('Content-Type', 'application/json');
        else
            return (new Response($users, 200)) ->header('Content-Type', 'application/json');
    }
    
    public function oneUser($id) {
         if( ! is_numeric($id))//proveri id
          { return (new Response(array('error'=>'id_nije_broj'), 404)) ->header('Content-Type', 'application/json');}
          
        $user=  \App\User::find($id);
        if($user===null)
        {
            return (new Response(array('error'=>'user_not_found', 'code'=>404), 404)) ->header('Content-Type', 'application/json');
        }else{
            
            return (new Response($user, 200)) ->header('Content-Type', 'application/json');
            
        }
    }
    
    public function addUser(Request $request){
        $user = new \App\User;

        $user->name     =  $request->name;
        $user->lastname =  $request->lastname;
        $user->email    =  $request->email;
        $user->password = bcrypt( $request->password);
        $user->username = $request->username;
        $user->rank     = $request->rank;
        $user->title    =$request->title;
        $user->date_of_birth=$request->date_of_birth;
        $user->jmbg     =$request->jmbg;
        $user->phone    =$request->phone;
        $user->city     =$request->city;
        $user->street   =$request->street;
        $user->street_num=$request->street_num;
        $user->bank     =$request->bank;
        $user->b_account=$request->b_account;
        $user->user_status=$request->user_status;
        $user->mac      =$request->mac;

        try{
            if($user->save())
                {
                UserController::saveFiles($user,$request);
                return (new Response(array('succes'=>'user_added'), 200)) ->header('Content-Type', 'application/json');
                }
        }  catch (\Illuminate\Database\QueryException $e){
            return (new Response(array('error'=>$e), 404)) ->header('Content-Type', 'application/json');
        }

    }
    
    public function deleteUser($id){     //ili hoces preko linka?   
        try{
            //$id= $request->id;
            if( ! is_numeric($id))//proveri id
          { return (new Response(array('error'=>'id_nije_broj'), 404)) ->header('Content-Type', 'application/json');}
         
            $user=  \App\User::find($id);
            if($user===null){
                return (new Response(array('error'=>'user_not_found'), 200)) ->header('Content-Type', 'application/json');
            }else{
                 if($user->delete()){                 
                 //delete files
                     //add TO DO path to file
                    if (File::exists(base_path().'/public/uploads/'.$user->pic_path))      {File::delete(base_path().'/public/uploads/'.$user->pic_path);}
                    if (File::exists(base_path().'/public/uploads/'.$user->doc_path1))     {File::delete(base_path().'/public/uploads/'.$user->doc_path1);}
                    if (File::exists(base_path().'/public/uploads/'.$user->doc_path2))     {File::delete(base_path().'/public/uploads/'.$user->doc_path2);}
                    if (File::exists(base_path().'/public/uploads/'.$user->doc_path3))     {File::delete(base_path().'/public/uploads/'.$user->doc_path3);}
                    //echo base_path().'\public\uploads\\'.$user->pic_path;
                    return (new Response(array('succes'=>'user_deleted'), 200)) ->header('Content-Type', 'application/json');
                }}
        } catch (\Illuminate\Database\QueryException $e){
            return (new Response(array('error'=>$e), 404)) ->header('Content-Type', 'application/json');
        }
     }
     

    public function modifyUser(Request $request,$id){    
        
        
        if( ! is_numeric($id))//proveri id
          { return (new Response(array('error'=>'id_nije_broj'), 404)) ->header('Content-Type', 'application/json');}
          
        $user = \App\User::find($id);
        
        if ($user === null) {//ako nepostoji user
          return (new Response(array('error'=>"user_nepostoji"), 404)) ->header('Content-Type', 'application/json');
        }else{
                $rules = array(
                    'email'            => 'required|email|unique:users,email,'.$id,  
                    'username'         => 'required|unique:users,username,'.$id, 
                      
                );
                $messages = [
                    'required' => ':attribute polje je obavezno.',
                    
                ];
                $validator = Validator::make($request->all(), $rules, $messages);//$messages
                if ($validator->fails()) {        
                    $r_messages = $validator->messages();
                    return (new Response(array('error'=>$r_messages, 'code'=>404), 404)) ->header('Content-Type', 'application/json');
                } else {
            
                        //echo 'add user metod';
                        //problem ako neposalje nesto a  on vec postoji u bazi on stavi null , brise
                        $user->name     =  $request->name;
                        $user->lastname =  $request->lastname;
                        $user->email    =  $request->email;
                        //$user->password = bcrypt( $request->password);//poseban metod za menjanje lozinke
                        $user->username = $request->username;
                        $user->rank     = $request->rank;       
                        $user->title    =$request->title;
                        $user->date_of_birth=$request->date_of_birth;
                        $user->jmbg      =$request->jmbg;
                        //$user->pic_path=$request->pic_path;
                        $user->phone    =$request->phone;
                        $user->city     =$request->city; 
                        $user->street   =$request->street;
                        $user->street_num=$request->street_num;
                        $user->bank     =$request->bank;
                        $user->b_account=$request->b_account;
                        //$user->doc_path1=$request->doc_path1;
                        //$user->doc_path2=$request->doc_path2;
                        //$user->doc_path3=$request->doc_path3;
                        $user->user_status=$request->user_status;
                        $user->mac      =$request->mac;   
                            try{
                                if($user->save())
                                    //to do modify files
                                    UserController::saveFiles($user,$request);
                                        //end modifying files
                                    return (new Response(array('succes'=>'user_modifyed'), 200)) ->header('Content-Type', 'application/json');

                            }  catch (\Illuminate\Database\QueryException $e){
                                return (new Response(array('error'=>$e), 404)) ->header('Content-Type', 'application/json');
                            }
                        }
        }        
     } 
     
     
    public function selfUser(Request $request) {
       
        if(array_key_exists('HTTP_AUTHORIZATION', $_SERVER)) {
           
            $token=substr($_SERVER['HTTP_AUTHORIZATION'], 7);
            try{
             
                $token2= JWTAuth::getPayload($token);

                $user=  \App\User::find($token2['id']);
                $permisions= $user->permissions;
                //permisije
                $jsonpermision=array();
                foreach ($permisions as $permision) {
                    //var_dump($permision->getPermisionName());
                    $jsonpermision[]=$permision->getPermisionName();
                   }
                //var_dump($jsonpermision);

                $user->permissions = $jsonpermision;
                $user->token       =$token;
                //rekvesti knjige
                $request=$user->requests_book;
                
                //rented books
                $rentList=array();
                foreach ($request as $get){
                    $rent=\App\Rented_books::find($get->id);
                    //var_dump($rent);
                    if($rent!=null)
                        $rentList[]=$rent;
                }
                $user->rented_books=$rentList;
                return (new Response($user, 200)) ->header('Content-Type', 'application/json');
             
            } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
                return response()->json(['token_expired'], $e->getStatusCode());
            } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
                return response()->json(['token_invalid'], $e->getStatusCode());
            }catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
                return response()->json(['token_absent'], $e->getStatusCode());
            }
        } else{
            return (new Response(array('error'=>'no_token', 'code'=>404), 404)) ->header('Content-Type', 'application/json');
        }
       
     }
     
    public function saveFiles($user,$request) {
        //var_dump(base_path());
        if (File::exists(base_path().'/public/uploads/'.$user->pic_path))      {File::delete(base_path().'/public/uploads/'.$user->pic_path);}
        if (File::exists(base_path().'/public/uploads/'.$user->doc_path1))     {File::delete(base_path().'/public/uploads/'.$user->doc_path1);}
        if (File::exists(base_path().'/public/uploads/'.$user->doc_path2))     {File::delete(base_path().'/public/uploads/'.$user->doc_path2);}
        if (File::exists(base_path().'/public/uploads/'.$user->doc_path3))     {File::delete(base_path().'/public/uploads/'.$user->doc_path3);}
        
        if ($request->hasFile('pic')) 
            {//save picture
                $file=$request->file('pic');
                $alowedSize=ConstraintsControler::getValue('pic_sizeMB');
                $fileSize=$file->getSize()/1024/1024;//bytes to MB
                //var_dump($alowedSize);
               // var_dump($fileSize);
                if( $fileSize>$alowedSize)
                {
                    return (new Response(array('error'=>'picture file too big'), 200)) ->header('Content-Type', 'application/json');    
                }else{
                    $extension=$file->getClientOriginalExtension();
                    //var_dump($extension);
//                    return (new Response(array('error'=>$extension), 200)) ->header('Content-Type', 'application/json');
//                    die();
                    if($extension=='jpg' || $extension=='png' 
                            || $extension=='jpeg' || $extension=='gif')
                    { 
                        $file->move(base_path().'/public/uploads/', 'picture_user_'. $user->id.".$extension");
                        $user->pic_path='picture_user_'. $user->id.".$extension";
                        //var_dump('picture_user_'. $user->id.".$extension");
                    }else{
                       return (new Response(array('error'=>'picture file extension not good'), 200)) ->header('Content-Type', 'application/json'); 
                    }
                }
            }
        $files = [];
        if ($request->file('doc1')) $files['doc1'] = $request->file('doc1');
        if ($request->file('doc2')) $files['doc2'] = $request->file('doc2');
        if ($request->file('doc3')) $files['doc3'] = $request->file('doc3');
        $alowedSize=ConstraintsControler::getValue('doc_sizeMB');

        //cv upitnik ostalo
        foreach ($files as $key=>$file)
        {   
            if(!empty($file)){
                $fileSize=$file->getSize()/1024/1024;//bytes to MB
                if( $fileSize>$alowedSize)
                {   
                    return (new Response(array('error'=>'doc file too big'), 200)) ->header('Content-Type', 'application/json');

                }  else {
                    $extension=$file->getClientOriginalExtension();
                    if($extension=='pdf' || $extension=='doc' || $extension=='docx' )
                    { 
                        $file->move(base_path().'/public/uploads/',$key.'_'. $user->id.".$extension");
                        
                        switch ($key) {
                            case 'doc1': $user->doc_path1=$key.'_'. $user->id.".$extension";
                                break;
                            case 'doc2': $user->doc_path2=$key.'_'. $user->id.".$extension";
                                break;
                            case 'doc3': $user->doc_path3=$key.'_'. $user->id.".$extension";
                                break;
                            default:
                                break;
                        }
                        //        $user->doc_path1=$request->doc_path1;
                        //        $user->doc_path2=$request->doc_path2;
                        //        $user->doc_path3=$request->doc_path3;
                    }else{
                        return (new Response(array('error'=>'document file extension not good'), 200)) ->header('Content-Type', 'application/json');
                    }

                }
            }
        }
    $user->save();
    }
    
    public function passwordChanger(Request $request) {
        $rules = array(  
            'oldpassword'         => 'required|min:6',
            'newpassword'         => 'required|min:6',
            'copypassword'        => 'required|min:6|same:newpassword',
            'username'            => 'required|exists:users,username',             
        );
        $messages = [
            'required' => ':attribute polje je obavezno.',            
            'exists'   => ':attribute ne postoji u bazi.',
            'min'      => ':attribute mora biti duže od 6 karaktera.',
            'same'     => ':attribute i :other moraju da se mečuju',
        ];
        $validator = Validator::make($request->all(), $rules, $messages);//$messages
        if ($validator->fails()) {        
            $r_messages = $validator->messages();
            return (new Response(array('error'=>$r_messages, 'code'=>404), 404)) ->header('Content-Type', 'application/json');
        } else {
           
                $token = JWTAuth::getToken();
                $token2=JWTAuth::getPayload($token);
                $user=  \App\User::find($token2['id']);
                //var_dump($user->password);
                //var_dump(bcrypt($request->oldpassword));
                //var_dump(bcrypt('sarasara'));
               // var_dump(Hash::check('sarasara', bcrypt('sarasara')));
                //var_dump(Hash::check($request->oldpassword ,$user->password));
                if(!Hash::check($request->oldpassword ,$user->password))
                    return response()->json(['error' => 'stari password nije tačan'], 401);
                
                $new=$request->newpassword;
                
                DB::beginTransaction();
                try{
                    $user->password = bcrypt($new);
                    $user->save();
                    DB::table('password_resets')->insert([
                                    'created_at' => date("Y-m-d",time()),
                                    'token'=>$token,
                                    'email'=>$user->email,
                                    ]);
                    DB::commit();    
                } catch(\Exception $e){
                        DB::rollback();
                        throw $e;
                }
                return response()->json(['success' => 'password je promenjen'], 401);         
            
        }
    }
    
    public function sync(Request $request) {
        //get token 
        $token = JWTAuth::getToken();
        $token2=JWTAuth::getPayload($token);
        $id=$token2['id'];
        $user=  \App\User::find($id);
        //get username,password,id,mac
        
        $responce=array(
            'username'=>$user->username,
            'id'=>$user->id,            
            'name'=>$user->name,
            'lastname'=>$user->lastname,
            'rank'=>$user->rank,
            'status'=>$user->user_status,
            'lab'=>Permissions::permisionExists($id,'lab'),//true false
            'mac'=>$user->mac);
         //return info
        return (new Response($responce, 200)) ->header('Content-Type', 'application/json');
       
    }
}
