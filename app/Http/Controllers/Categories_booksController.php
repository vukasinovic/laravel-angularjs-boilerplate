<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class Categories_booksController extends Controller
{
    public function addCategoryBook ( Request $request){
        $name=$request->name;
        if($name==null)
        { return (new Response(array('error'=>'name_empty'), 404)) ->header('Content-Type', 'application/json');}    
        $cat=new \App\Categories_books;
        $cat->name=$name;
        try{
            $cat->save();
            return (new Response(array('succes'=>'category_books_added'), 200)) ->header('Content-Type', 'application/json');
        } catch (\Illuminate\Database\QueryException $e){
            return (new Response(array('error'=>$e), 404)) ->header('Content-Type', 'application/json');
        } 
    }
    public function modifyCategoryBook (Request $request){
        $idcat=$request->id_cat;
        $nameNew=$request->name;
        if( ! is_numeric($idcat))//proveri id
        { return (new Response(array('error'=>'id_nije_broj'), 404)) ->header('Content-Type', 'application/json');}
         if($nameNew==null)
        { return (new Response(array('error'=>'name_empty'), 404)) ->header('Content-Type', 'application/json');}    
        
        $cat= \App\Categories_books::find($idcat);
        $cat->name=$nameNew;
        
        try{
            $cat->save();
            return (new Response(array('succes'=>'category_books_modifyed'), 200)) ->header('Content-Type', 'application/json');
        } catch (\Illuminate\Database\QueryException $e){
            return (new Response(array('error'=>$e), 404)) ->header('Content-Type', 'application/json');
        }
    }
    public function deleteCategoryBook ( $idcat){
        //$idcat=$request->id_cat;
        if( ! is_numeric($idcat))//proveri id
        { return (new Response(array('error'=>'id_nije_broj'), 404)) ->header('Content-Type', 'application/json');}
         $cat= \App\Categories_books::find($idcat);
         try{
         $cat->delete();
          return (new Response(array('succes'=>'category_books_deleted'), 200)) ->header('Content-Type', 'application/json');
        } catch (\Illuminate\Database\QueryException $e){
            return (new Response(array('error'=>$e), 404)) ->header('Content-Type', 'application/json');
        }
    }
    public function allCategoriesBook (){
        $categories=  \App\Categories_books::all();
        return (new Response($categories, 200)) ->header('Content-Type', 'application/json');
    }
   
}

