<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JWTAuth;
use DB;
use Illuminate\Http\Response;
use App\Requests_books;
//use Illuminate\Http\Response;
class Rented_devicesControler extends Controller
{
     public function allRentedDevices() {
         $x= \App\Rented_devices::all();
        if( $x===null)
            return (new Response(array('error'=>'rented_devices_not_found', 'code'=>404), 404)) ->header('Content-Type', 'application/json');
        else
        return (new Response($x, 200)) ->header('Content-Type', 'application/json');
    }
    
    public function approvedList (){
        $list= \App\Rented_devices::where('rent_status', 'approved')
                ->orWhere('rent_status', 'overdue')->get();        
        foreach ($list as $req ) {
           
            $request=$req->request;
      
            $user=\App\User::find($request->user_id);
            $request->user_name=$user->name;
            $request->user_lastname=$user->lastname;
            
            $device=  \App\Device::find($request->device_id);            
            $request->type=$device->type;
            $request->description=$device->description;
           
        }
        if( $list===null)
            return (new Response(array('error'=>'list_empty', 'code'=>404), 404)) ->header('Content-Type', 'application/json');
        else
            return (new Response($list, 200)) ->header('Content-Type', 'application/json');
    
    }
    
    public function updateRentedtDevice(Request $request) {
        $id_rent=$request->id_rent;
        $status=$request->status;
        if( ! is_numeric($id_rent))//proveri id
          { return (new Response(array('error'=>'id_nije_broj'), 404)) ->header('Content-Type', 'application/json');}
         
        if( $status==="returned" || $status==="overdue")  {
            //to do mechanizam for updating to overdue
                     
            try {
                $rent= \App\Rented_devices::find($id_rent);
                if($rent ==null)
                     {return (new Response(array('error'=>'rent_nepostoji'), 200)) ->header('Content-Type', 'application/json');}
                $rent->rent_status  =$status;
                if($status==="returned"){
                   $rent->returned_at =date("Y-m-d H:i:s");
                   $request=$rent->request;
                   //var_dump($request->book_id);
                   //promenimo status knjige
                   $device= \App\Device::find($request->device_id);
                   $device->device_status='free';
                   $device->save();
                }
                $rent->saveOrFail();
                return (new Response(array('succes'=>'request_finished','status'=>$status), 200)) ->header('Content-Type', 'application/json');
                
            } catch (\Illuminate\Database\QueryException $e) {
                return (new Response(array('error'=>$e), 404)) ->header('Content-Type', 'application/json');
            }
        }else{
            { return (new Response(array('error'=>'status_not_correct'), 404)) ->header('Content-Type', 'application/json');}
        }
    }
}

