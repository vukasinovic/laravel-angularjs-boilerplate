<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JWTAuth;
use DB;
use Illuminate\Http\Response;
use App\Requests_books;
//use Illuminate\Http\Response;
class Rented_booksControler extends Controller
{
    public function allRentedBooks() {
         $x= \App\Rented_books::all();
//        $x = DB::table('rented_books')
//           // ->select('id_rented_book as id_rent , request_id_fk as id_request ,user_id , book_id , request_status , rent_status')
//            ->leftJoin('requests_books', 'rented_books.request_id_fk', '=', 'requests_books.id')
//            ->get();
        if( $x===null)
            return (new Response(array('error'=>'rented_books_not_found', 'code'=>404), 404)) ->header('Content-Type', 'application/json');
        else
        return (new Response($x, 200)) ->header('Content-Type', 'application/json');
    }

    public function approvedList (){
        $list= \App\Rented_books::where('rent_status', 'approved')
                ->orWhere('rent_status', 'overdue')->get();        
        foreach ($list as $req ) {
           
            $request=$req->request;
      
            $user=\App\User::find($request->user_id);
            $request->user_name=$user->name;
            $request->user_lastname=$user->lastname;
            
            $book=\App\Book::find($request->book_id);  
            if($book!=null){                      
            $request->book_name=$book->name;
            $request->book_autors=$book->pullAutors();
            }
        }
        if( $list===null)
            return (new Response(array('error'=>'list_empty', 'code'=>404), 404)) ->header('Content-Type', 'application/json');
        else
            return (new Response($list, 200)) ->header('Content-Type', 'application/json');
    
    }
    
    public function updateRentedtBook(Request $request) {
        $id_rent=$request->id_rent;
        $status=$request->status;
        
        if( ! is_numeric($id_rent))//proveri id
          { return (new Response(array('error'=>'id_nije_broj'), 404)) ->header('Content-Type', 'application/json');}
         
        if( $status==="returned" || $status==="overdue")  {
            //to do mechanizam for updating to overdue
                     
            try {
                $rent= \App\Rented_books::find($id_rent);
                if($rent ==null)
                     {return (new Response(array('error'=>'rent_nepostoji'), 200)) ->header('Content-Type', 'application/json');}
                $rent->rent_status  =$status;
                if($status==="returned"){
                   $rent->returned_at =date("Y-m-d H:i:s");
                   $request=$rent->request;
                   //var_dump($request->book_id);
                   //promenimo status knjige
                   $book= \App\Book::find($request->book_id);
                   $book->book_status='free';
                   $book->save();
                }
                $rent->saveOrFail();
                return (new Response(array('succes'=>'request_finished','status'=>$status), 200)) ->header('Content-Type', 'application/json');
                
            } catch (\Illuminate\Database\QueryException $e) {
                return (new Response(array('error'=>$e), 404)) ->header('Content-Type', 'application/json');
            }
        }else{
            { return (new Response(array('error'=>'status_not_correct'), 404)) ->header('Content-Type', 'application/json');}
        }
    }
}