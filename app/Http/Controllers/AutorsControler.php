<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class AutorsControler extends Controller
{
    public function addAutor (Request $request){
        $name=$request->name;
        $lastname=$request->lastname;
        $title=$request->title;
        
        if($name==null || $lastname==null)
        { return (new Response(array('error'=>'name_or_lastname_empty'), 404)) ->header('Content-Type', 'application/json');}    
        $autor=new \App\Autor;
        $autor->title=$title;
        $autor->a_name=$name;
        $autor->a_last_name=$lastname;
        try{
            $autor->save();
            return (new Response(array('succes'=>'autor_added'), 200)) ->header('Content-Type', 'application/json');
        } catch (\Illuminate\Database\QueryException $e){
            return (new Response(array('error'=>$e), 404)) ->header('Content-Type', 'application/json');
        } 
    }
    
    public function modifyAutor (Request $request){
        $idAutor=$request->id_autor;
        $name=$request->name;
        $lastname=$request->lastname;
        $title=$request->title;
        if( ! is_numeric($idAutor))//proveri id
        { return (new Response(array('error'=>'id_nije_broj'), 404)) ->header('Content-Type', 'application/json');}
         if($name==null || $lastname==null)
        { return (new Response(array('error'=>'name_or_lastname_empty'), 404)) ->header('Content-Type', 'application/json');}    
        
        $autor=\App\Autor::find($idAutor);
        $autor->title=$title;
        $autor->a_name=$name;
        $autor->a_last_name=$lastname;
        
        try{
             $autor->save();
            return (new Response(array('succes'=>'autor_modifyed'), 200)) ->header('Content-Type', 'application/json');
        } catch (\Illuminate\Database\QueryException $e){
            return (new Response(array('error'=>$e), 404)) ->header('Content-Type', 'application/json');
        }
    }
    
    public function deleteAutor ($idAutor){
        //$idAutor=$request->id_autor;
        if( ! is_numeric($idAutor))//proveri id
        { return (new Response(array('error'=>'id_nije_broj'), 404)) ->header('Content-Type', 'application/json');}
         $a= \App\Autor::find($idAutor);
         try{
         $a->delete();
          return (new Response(array('succes'=>'autor_deleted'), 200)) ->header('Content-Type', 'application/json');
        } catch (\Illuminate\Database\QueryException $e){
            return (new Response(array('error'=>$e), 404)) ->header('Content-Type', 'application/json');
        }
    }
    
    public function allAutors (){
        $autors= \App\Autor::all();
        return (new Response($autors, 200)) ->header('Content-Type', 'application/json');
    }
}


