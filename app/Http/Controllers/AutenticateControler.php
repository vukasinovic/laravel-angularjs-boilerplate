<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\User;
use DB;
use Validator; 
class AutenticateControler extends Controller
{
    public function authenticate(Request $request)
    {
        $ip=$_SERVER['REMOTE_ADDR'];//ip adresa
        // grab credentials from the request

        $credentials = $request->only('username', 'password');

        $user= DB::table('users')
            ->where(['username'=>$request->username],['password'=>bcrypt($request->password)])
            ->first();
        //check if user exists
        if ($user===null)
        {
                return response()->json(['error' => 'user doesnt exist'], 401);
        }

        $rank=$user->rank;
        $username=$user->username;
        $password=$request->password;
        $id=$user->id;
        $name=$user->name;
        $lastname=$user->lastname;
        $customClaims = ['ip' => $ip,'id'=>$id,'rank'=>$rank,'name'=>$name,'lastname'=>$lastname,'username'=>$username];

        // grab credentials from the request
        //$credentials = $request->only('email', 'password');
        // Change email to username
        $credentials = $request->only('username', 'password');

        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials, $customClaims)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        // all good so return the token
        return response()->json(compact('token'));
    }
}
