<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JWTAuth;
use DB;
use Illuminate\Http\Response;
use App\Requests_devices;
use App\Rented_devices;
//use Illuminate\Http\Response;
class Requests_devicesControler extends Controller
{//test all
    public function requestsDevicesAll(Request $request) {
        //echo 'requet all for books';
        $requestsD= \App\Requests_devices::all();
        
        if($requestsD===null)
            return (new Response(array('error'=>'no_requests_found', 'code'=>404), 404)) ->header('Content-Type', 'application/json');
        else
            return (new Response($requestsD, 200)) ->header('Content-Type', 'application/json');
    
    }

    
    public function requestDevice(Request $request) {
        $id_device=$request->id_device;
        //echo 'request a book'.$id_book; 
        if( ! is_numeric($id_device))//proveri id
          { return (new Response(array('error'=>'id_nije_broj'), 404)) ->header('Content-Type', 'application/json');}
        $token = JWTAuth::getToken();
        $token2=JWTAuth::getPayload($token);
        $user=  \App\User::find($token2['id']);
        $user_id = $token2['id'];
         if($user===null)
        {
            return (new Response(array('error'=>'user_not_found', 'code'=>404), 404)) ->header('Content-Type', 'application/json');
        }else{
            $device= \App\Device::find($id_device);
            if($device===null)
            {
                return (new Response(array('error'=>'device_not_found'), 404)) ->header('Content-Type', 'application/json');
            }
            try{
               
                $id_exists= \App\Requests_devices::where('user_id' , '=', $user_id)
                        ->where('device_id' , '=', $id_device) 
                        ->where('status', '=','wait')
                        ->count();
                //var_dump($id_exists[0]['attributes']);
                //var_dump($id_exists);
                if($id_exists>0){
                   return (new Response(array('error'=>'request_exists'), 200)) ->header('Content-Type', 'application/json');
                }else{
                    $request=new \App\Requests_devices();
                    $request->user_id =$user_id;
                    $request->device_id =$id_device;
                    $request->status  ="wait";
                    $request->created_at=date("Y-m-d H:i:s");
                    $device->device_status='rented';
                    $device->save();
                    $request->save();
                    return (new Response(array('succes'=>'request_given'), 200)) ->header('Content-Type', 'application/json');
                } 
            } catch (\Illuminate\Database\QueryException $e){
                    return (new Response(array('error'=>$e), 404)) ->header('Content-Type', 'application/json');
                }
        }  
    }
    
    public function finalizeRequestDevice( Request $request) {
        $id_request=$request->id_request;
        $status=$request->status;
        
        if( ! is_numeric($id_request))//proveri id
          { return (new Response(array('error'=>'id_nije_broj'), 404)) ->header('Content-Type', 'application/json');}
         
        if( $status==="denied" || $status==="approved")  {
            
             $req= \App\Requests_devices::find($id_request);
             //var_dump($req);
             
            if($req==null)
                { return (new Response(array('error'=>'id_ne_postoji'), 404)) ->header('Content-Type', 'application/json');}
            try {
                
                $req->status =$status;
                $req->finalized_at =date("Y-m-d H:i:s");
                $req->saveOrFail();
                
                if($status==="approved"){
                    $id_exists= \App\Rented_devices::where('request_id_fk' , '=', $id_request)
                        ->where('rent_status', '=','approved')
                        ->count();
                    if($id_exists>0)
                        {return (new Response(array('error'=>'rent_exists'), 200)) ->header('Content-Type', 'application/json');}
                    
                    $rent=new \App\Rented_devices();
                    $rent->request_id_fk=$id_request;
                    $rent->rent_status     ='approved';
                    
                    
                    $rok=ConstraintsControler::getValue('deadline');
                    //echo $rok;
//                    echo date("Y-m-d H:i:s");                    
//                    echo "<br>";
//                    echo date("Y-m-d H:i:s",time()+ ( $rok * 24 * 60 * 60));
                    $rent->return_date=date("Y-m-d",time()+ ( $rok * 24 * 60 * 60));
                    
                    $rent->saveOrFail();
                }
                
                return (new Response(array('succes'=>'request_finished','status'=>$status), 200)) ->header('Content-Type', 'application/json');
                
            } catch (\Illuminate\Database\QueryException $e) {
                return (new Response(array('error'=>$e), 404)) ->header('Content-Type', 'application/json');
            }
        }else{
            { return (new Response(array('error'=>'status_not_correct'), 404)) ->header('Content-Type', 'application/json');}
        }
          
       
    }

   
    public function waitList() {
        $list= \App\Requests_devices::where('status', 'wait')
                ->get();   
        foreach ($list as $request ) {
          
            $user=\App\User::find($request->user_id);
            $request->user_name=$user->name;
            $request->user_lastname=$user->lastname;
            
            $device=  \App\Device::find($request->device_id);            
            $request->type=$device->type;
            $request->description=$device->description;
           
        }
        if( $list===null)
            return (new Response(array('error'=>'list_empty', 'code'=>404), 404)) ->header('Content-Type', 'application/json');
        else
            return (new Response($list, 200)) ->header('Content-Type', 'application/json');
    
    }
    
}
