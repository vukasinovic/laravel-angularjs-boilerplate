<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JWTAuth;
use DB;
use Illuminate\Http\Response;
use App\Requests_books;
//use Illuminate\Http\Response;
class Requests_bookControler extends Controller
{
    public function requestsBooksAll(Request $request) {
        //echo 'requet all for books';
        $requestsB= \App\Requests_books::all();
        
        if($requestsB===null)
            return (new Response(array('error'=>'no_requests_found', 'code'=>404), 404)) ->header('Content-Type', 'application/json');
        else
            return (new Response($requestsB, 200)) ->header('Content-Type', 'application/json');
    
    }
    public function requestBook(Request $request) {
        //echo 'request a book'.$id_book; 
        $id_book=$request->id_book;
        if( ! is_numeric($id_book))//proveri id
          { return (new Response(array('error'=>'id_nije_broj'), 404)) ->header('Content-Type', 'application/json');}
        $token = JWTAuth::getToken();
        $token2=JWTAuth::getPayload($token);
        $user=  \App\User::find($token2['id']);
        $user_id = $token2['id'];
         if($user===null)
        {
            return (new Response(array('error'=>'user_not_found', 'code'=>404), 404)) ->header('Content-Type', 'application/json');
        }else{
            $book= \App\Book::find($id_book);
            if($book===null)
            {
                return (new Response(array('error'=>'book_not_found'), 404)) ->header('Content-Type', 'application/json');
            }
            try{
               
                $id_exists= \App\Requests_books::where('user_id' , '=', $user_id)
                        ->where('book_id' , '=', $id_book) 
                        ->where('request_status', '=','wait')
                        ->count();
                //var_dump($id_exists[0]['attributes']);
                //var_dump($id_exists);
                if($id_exists>0){
                   return (new Response(array('error'=>'request_exists'), 200)) ->header('Content-Type', 'application/json');
                }else{
                    $requestB=new \App\Requests_books;
                    $requestB->user_id =$user_id;
                    $requestB->book_id =$id_book;
                    $requestB->request_status  ='wait';
                    $requestB->created_at=date("Y-m-d H:i:s");
                    $book->book_status='rented';
                    $book->save();
                    $requestB->save();
                    //sendMail::mailnow($user,'novi_zahtev_knjiga',$book);
                    return (new Response(array('succes'=>'request_given'), 200)) ->header('Content-Type', 'application/json');
                } 
            } catch (\Illuminate\Database\QueryException $e){
                    return (new Response(array('error'=>$e), 404)) ->header('Content-Type', 'application/json');
                }
        }  
    }
    public function finalizeRequestBook( Request $request) {
        $id_request=$request->id_request;
        $status=$request->status;
        
        if( ! is_numeric($id_request))//proveri id
          { return (new Response(array('error'=>'id_nije_broj'), 404)) ->header('Content-Type', 'application/json');}
         
        if( $status==="denied" || $status==="approved")  {
            
             $req= \App\Requests_books::find($id_request);
             //var_dump($req);
             
            if($req==null)
                { return (new Response(array('error'=>'id_ne_postoji'), 404)) ->header('Content-Type', 'application/json');}
            try {
                
                $req->request_status       =$status;
                $req->finalized_at =date("Y-m-d H:i:s");
                $req->saveOrFail();
                
                if($status==="approved"){
                    $id_exists= \App\Rented_books::where('request_id_fk' , '=', $id_request)
                        ->where('rent_status', '=','approved')
                        ->count();
                    if($id_exists>0)
                        {return (new Response(array('error'=>'rent_exists'), 200)) ->header('Content-Type', 'application/json');}
                    
                    $rent=new \App\Rented_books();
                    $rent->request_id_fk=$id_request;
                    $rent->rent_status     ='approved';
                    
                    
                    $rok=ConstraintsControler::getValue('deadline');
                    //echo $rok;
//                    echo date("Y-m-d H:i:s");                    
//                    echo "<br>";
//                    echo date("Y-m-d H:i:s",time()+ ( $rok * 24 * 60 * 60));
                    $rent->return_date=date("Y-m-d",time()+ ( $rok * 24 * 60 * 60));
                    
                    $rent->saveOrFail();
                }
                
                return (new Response(array('succes'=>'request_finished','status'=>$status), 200)) ->header('Content-Type', 'application/json');
                
            } catch (\Illuminate\Database\QueryException $e) {
                return (new Response(array('error'=>$e), 404)) ->header('Content-Type', 'application/json');
            }
        }else{
            { return (new Response(array('error'=>'status_not_correct'), 404)) ->header('Content-Type', 'application/json');}
        }
          
       
    }

    public function waitList() {       
        $list= \App\Requests_books::where('request_status', 'wait')
                ->get();   
        
        foreach ($list as $request ) {
            
            $user=\App\User::find($request->user_id);
            $request->user_name=$user->name;
            $request->user_lastname=$user->lastname;
            //var_dump($request->book_id);
            $book=\App\Book::where('id_book', $request->book_id)->first();  
            //$book=\App\Book::find($request->book_id); 
            if($book!=null){
            $request->book_name=$book->name;
            $request->book_autors=$book->pullAutors();
            }
        }
        if( $list===null)
            return (new Response(array('error'=>'list_empty', 'code'=>404), 404)) ->header('Content-Type', 'application/json');
        else
            return (new Response($list, 200)) ->header('Content-Type', 'application/json');
    
    }
    
    
}
