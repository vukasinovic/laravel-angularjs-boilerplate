<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Device;
use Validator;
use DB;
class DeviceController extends Controller
{
    public function allDevices() {
        $devices= \App\Device::all();
        
         foreach ($devices as $device) {
            $device->categories_device();            
        }
        
        if( $devices===null)
            return (new Response(array('error'=>'devices_not_found', 'code'=>404), 404)) ->header('Content-Type', 'application/json');
        else
        return (new Response($devices, 200)) ->header('Content-Type', 'application/json');
    }
    
    public function oneDevice($id) {
        $device=  \App\Device::find($id);
        $device->categories_device();    
        //return $device->toJson();
        if($device===null)
            return (new Response(array('error'=>'device_not_found', 'code'=>404), 404)) ->header('Content-Type', 'application/json');
        else
        return (new Response($device, 200)) ->header('Content-Type', 'application/json');
    }
    
    public function addDevice(Request $request) {
         $rules = array(
            'type' => 'required',            
            'description' => 'required',
            'index' => 'required',
        );
        $messages = [
            'required' => ':attribute polje je obavezno.',
           
        ];
        $validator = Validator::make($request->all(), $rules, $messages);//$messages
        if ($validator->fails()) {        
            $r_messages = $validator->messages();
            return (new Response(array('error'=>$r_messages, 'code'=>404), 404)) ->header('Content-Type', 'application/json');
        } else {
                $device = new Device;

                $device->type = $request->type;
                $device->description = $request->description;
                $device->mac = $request->mac;
                $device->serial_num = $request->serial_num;
                $device->os_vresion = $request->os_vresion;
                $device->dimensions = $request->dimensions;
                $device->extra_equipment = $request->extra_equipment;
                $device->device_status = $request->device_status;
                $device->index = $request->index;

                try{
                   
                        if($device->save()){
                            //save categories for device
                            foreach ($request->categories_device as $cat)
                                DB::table('device_to_category')->insert([
                                    'device_id' => $device->id_device,
                                    'categories_device_id'=>$cat
                                    ]);
                            return (new Response(array('succes'=>'device_added'), 200)) ->header('Content-Type', 'application/json');
                        }else{
                            return (new Response(array('error'=>'failed_to_add_device'), 404)) ->header('Content-Type', 'application/json');
                        }
                       
                } catch (\Illuminate\Database\QueryException $e){
                    return (new Response(array('error'=>$e), 404)) ->header('Content-Type', 'application/json');
                } 
               
        }
    }
    
    public function modifyDevice(Request $request) {
        $id=$request->id_device;
        if( ! is_numeric($id))//proveri id
          { return (new Response(array('error'=>'id_nije_broj'), 404)) ->header('Content-Type', 'application/json');}
         
        $device = \App\Device::find($id);
        
        if ($device === null) {//ako nepostoji device
          return (new Response(array('error'=>"device_nepostoji"), 404)) ->header('Content-Type', 'application/json');
        }else{
            $rules = array(
                    'type'        => 'required',
                    //'category'    => 'required',
                    'description' => 'required', 
                    'index' => 'required',
                );
            $messages = [
                    'required' => ':attribute polje je obavezno.',
                    
                ];
            $validator = Validator::make($request->all(), $rules, $messages);//$messages
                if ($validator->fails()) {        
                    $r_messages = $validator->messages();
                    return (new Response(array('error'=>$r_messages, 'code'=>404), 404)) ->header('Content-Type', 'application/json');
                } else {
                    $device->type = $request->type;
                    //$device->category = $request->category;
                    $device->description = $request->description;
                    $device->mac = $request->mac;
                    $device->serial_num = $request->serial_num;
                    $device->os_vresion = $request->os_vresion;
                    $device->dimensions = $request->dimensions;
                    $device->extra_equipment = $request->extra_equipment;
                    $device->device_status = $request->device_status;
                    $device->index = $request->index;
                    try{
                       
                            if($device->save()){
                                //kategorije
                                DB::table('device_to_category')->
                                    where('device_id' , '=', $device->id_device)->
                                    delete();   //izbrisi prethodne kategorije
                                
                                foreach ($request->categories_device as $cat)
                                    DB::table('device_to_category')->insert([
                                    'device_id' => $device->id_device,
                                    'categories_device_id'=>$cat
                                    ]);//dodaj nove kategorije
                                return (new Response(array('succes'=>'device_modifyed'), 200)) ->header('Content-Type', 'application/json');
                             
                             }
                           
                    }  catch (\Illuminate\Database\QueryException $e){
                        return (new Response(array('error'=>$e), 404)) ->header('Content-Type', 'application/json');
                    }
                }
        }
    }
    public function deleteDevice($id,Request $request) {
         try{
            if( ! is_numeric($id))//proveri id
            { return (new Response(array('error'=>'id_nije_broj'), 404)) ->header('Content-Type', 'application/json');}
         
            $device= \App\Device::find($id);
            if($device===null){
                return (new Response(array('error'=>'device_not_found'), 200)) ->header('Content-Type', 'application/json');
            }else{
                
                    //delete categories bonds
                    DB::table('device_to_category')->where('device_id', $request->id)->delete();
                    if($device->delete()){  
                        return (new Response(array('succes'=>'device_deleted'), 200)) ->header('Content-Type', 'application/json');
                    }
                
            }
        } catch (\Illuminate\Database\QueryException $e){
            return (new Response(array('error'=>$e), 404)) ->header('Content-Type', 'application/json');
        } 
    }
}
