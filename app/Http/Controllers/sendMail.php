<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\Http\Requests;


class sendMail extends Controller
{//https://laravel.com/docs/5.2/mail
    //podesi mail setings
    public function mailnow($user,$why,$thing) {
        $mailtype;
        switch ($why) {
            case 'novi_zahtev_knjiga':
                $mailtype='mail1';
                break;
            case 'novi_zahtev_uredjaj':
                $mailtype='mail2';
                break;
            case 'knjiga_vec_izdata':
                $mailtype='mail3';
                break;
            case 'uredjaj_vec_izdat':
                $mailtype='mail4';
                break;
            case 'knjiga_iznajmljivanje_odobreno':
                $mailtype='mail5';
                break;
            case 'uredjaj_iznajmljivanje_odobreno':
                $mailtype='mail6';
                break;
            case 'knjiga_kasni_vracanje':
                $mailtype='mail7';
                break;
            case 'uredjaj_kasni_vracanje':
                $mailtype='mail8';
                break;
            default:
                return;
                break;
        }
        //get thing objasnjenje koji objekat knjiga ili uredjaj
        
        $mailtext=ConstraintsControler::getValue($mailtype);
        $mailPotpis=ConstraintsControler::getValue('mail0');
        $adminAdresa=ConstraintsControler::getValue('admin_mail');
        $useradresa=$user->email;
        $username=$user->lastname+''+$user->name;
        $data=array($user,$mailtext,$mailPotpis);
        //admin-za odobravanje iznajmljivanje knjige ili uredjaja 
        //clan- novi registrovani korisnik
        //admin- imate stvari za odobravanje
        //clan- odobreno iznajmljivanje ili nije odobreno
        //https://laravel.com/docs/5.2/blade
        Mail::send('emails', $data, function ($message) {
            $message->from($adminAdresa, 'Biblioteka VTŠAppsTim');

            $message->to($useradresa,$username);
        });
        return;
        
    }
    public function mailauto($param) {
        //clan- kasni sa vracanjem 
        
    }
}
