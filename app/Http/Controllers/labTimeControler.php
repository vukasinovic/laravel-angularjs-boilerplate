<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class labTimeControler extends Controller
{
    //sta god milosu treba
    public function addLabTime(Request $request) {
        $rules = array(
        'date' => 'required',   
        'time' => 'required',
        'user_id' => 'required',//user id
        );
        $messages = [
            'required' => ':attribute polje je obavezno.',            
        ];
        $validator = Validator::make($request->all(), $rules, $messages);//$messages
        if ($validator->fails()) {        
            $r_messages = $validator->messages();
            return (new Response(array('error'=>$r_messages, 'code'=>404), 404)) ->header('Content-Type', 'application/json');
        } else {
            try{
                
                    if( DB::table('labtime')->insert([
                            'date' => $request->date,
                            'time'=>$request->time,
                            'user_id'=>$request->user_id
                            ])
                            ){
                    return (new Response(array('succes'=>'lab time added'), 200)) ->header('Content-Type', 'application/json');
                    }
                              
            }catch (\Illuminate\Database\QueryException $e){
                return (new Response(array('error'=>$e), 404)) ->header('Content-Type', 'application/json');
            }
        }
    }
    
    public function addUnauththorizedAccess(Request $request) {
        $rules = array(
            'mac' => 'required',   
            'time' => 'required',
            //'user_id' => 'required',//user id
            //'username' => 'required',
            
        );
        $messages = [
            'required' => ':attribute polje je obavezno.',            
        ];
        $validator = Validator::make($request->all(), $rules, $messages);//$messages
        if ($validator->fails()) {        
            $r_messages = $validator->messages();
            return (new Response(array('error'=>$r_messages, 'code'=>404), 404)) ->header('Content-Type', 'application/json');
        } else {
            try{
                
                    if( DB::table('unauththorized_access')->insert([
                            'mac' => $request->mac,
                            'time'=>$request->time,
                            'user_id'=>$request->user_id,
                            'username'=>$request->username,
                            'description'=>$request->description
                            ])
                            ){
                    return (new Response(array('succes'=>'unauththorized access added'), 200)) ->header('Content-Type', 'application/json');
                    }
                              
            }catch (\Illuminate\Database\QueryException $e){
                return (new Response(array('error'=>$e), 404)) ->header('Content-Type', 'application/json');
            }
        }
    }
    
    public function addSyncRpi(Request $request) {
        $rules = array(
            'type' => 'required',   
            'query' => 'required', 
            'time' => 'required',
            'user_id' => 'required',//user id
            'username' => 'required',
            
        );
        $messages = [
            'required' => ':attribute polje je obavezno.',            
        ];
        $validator = Validator::make($request->all(), $rules, $messages);//$messages
        if ($validator->fails()) {        
            $r_messages = $validator->messages();
            return (new Response(array('error'=>$r_messages, 'code'=>404), 404)) ->header('Content-Type', 'application/json');
        } else {
            try{
                
                    if( DB::table('unauththorized_access')->insert([
                            'type' => $request->type,
                            'time'=>$request->time,
                            'user_id'=>$request->user_id,
                            'query'=>$request->query,
                            'username'=>$request->username
                            ])
                            ){
                    return (new Response(array('succes'=>'unauththorized access added'), 200)) ->header('Content-Type', 'application/json');
                    }
                              
            }catch (\Illuminate\Database\QueryException $e){
                return (new Response(array('error'=>$e), 404)) ->header('Content-Type', 'application/json');
            }
        }
    }
}
