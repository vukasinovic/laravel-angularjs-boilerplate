<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Illuminate\Http\Response;

//use Illuminate\Http\Response;
class ConstraintsControler extends Controller
{
    public function edit(Request $request) {
        DB::table('constrains')->where('value_name', $request->value)
            ->update(['const_value' => $request->const]);
        return (new Response(array('succes'=>$request->value.' changed to '.$request->const), 200)) ->header('Content-Type', 'application/json');
    }
    
    public function all() {
        
        $x=DB::table('constrains')->select('value_name as value', 'const_value as const')->get();
        return (new Response($x, 200)) ->header('Content-Type', 'application/json');
    }
    
    public function specific($value) {
        
        $x=DB::table('constrains')->select('value_name as value', 'const_value as const')->where('value_name', $value)->get();
        return (new Response($x, 200)) ->header('Content-Type', 'application/json');
    }
    
    public static function getValue($value) {
        
        $x=DB::table('constrains')->select('const_value as const')->where('value_name', $value)->first();
        $return=$x->const;
        
        return $return;
    }
    
    public function getConst($value) {
        $x=DB::table('constrains')->select( 'const_value as const')->where('value_name', $value)->get();
        return ($x);
    }
}

