<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','username', 'email', 'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',       
        "status_mac",
        "prijava",
        "odjava",
        "mac_synced",
        "stat_mac_sync",
        
    ];
    public function permissions(){
        return $this->hasMany('App\Permissions');
    }
    public function requests_book(){
        return $this->hasMany('App\Requests_books');
    }
    
   public function getId(){
       return $this['attributes']['id'];
   }
   public function saveToken($id,$tokenJti) {
        $userSave = User::find($id);
        $userSave->remember_token=$tokenJti;
        $userSave->save();
        return;
   }
}
