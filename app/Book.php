<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    //
    //protected $table='books';
    protected $primaryKey='id_book';
    //protected $dateFormat='U';
    protected $fillable = [
        'name',
        'language',
        'year_of_publication',
        'isbn',
        'description',
        'edition',
        'purchase',
        'price',
        'binding',        
        'book_status',
        'electronic_book',
        'book_src',
        'lastname'
    ];
    
    public function requests_book(){
        return $this->hasMany('App\Requests_books');
    }
    
    public function categories_book(){
        return $this->belongsToMany('App\Categories_books','book_to_category');
    }
    
    public function autors(){
        return $this->belongsToMany('App\Autor','book_to_autor');
    }
    public function pullAutors(){
        $autors=$this->autors;
        $list=array();
        foreach ($autors as $autor){
//                var_dump($autor);
//                echo $autor->a_name;
//                echo $autor->a_last_name;
//                echo $autor->title;
                $list[]=array('title'=> $autor->title,
                        'lastname'=>$autor->a_last_name,
                        'name'=>$autor->a_name                      
                );
            }
        return $list;
    }
}
