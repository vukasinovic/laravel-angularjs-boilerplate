<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Requests_devices extends Model
{
   
    protected $fillable = [
        'user_id',
        'request_status',
        'device_id',
        'finalized_at',
        'status'
       
    ];
    public $timestamps  = false;
    public function user()
    {
        return $this->belongsTo('App\User','id','user_id');
    }
    public function device()
    {
        return $this->belongsTo('App\Device','id_device','device_id');
    }
}