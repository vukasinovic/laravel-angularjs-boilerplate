<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Requests_books extends Model
{
   
    protected $fillable = [
        'user_id',
        'request_status',
        'book_id',
        'finalized_at',
        'created_at'
       
    ];
    public $timestamps  = false;
    public function user()
    {
        return $this->belongsTo('App\User','id','user_id');
    }
    public function book()
    {
        return $this->belongsTo('App\Book','id_book','book_id');
    }
}
