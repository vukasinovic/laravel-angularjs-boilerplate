<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rented_devices extends Model
{
     public  $primaryKey='id_rented_device';
     protected $fillable = [
        'request_id_fk',
        'rent_status',
        'return_date'  
       
    ];
     public $timestamps  = false;
    public function request()
    {
        return $this->hasOne('App\Requests_devices','id','request_id_fk');
    } 
    
}

