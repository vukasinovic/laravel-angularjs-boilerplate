<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Autor extends Model
{
    public $timestamps = false;
    protected $primaryKey='id_autor';
     
    public function books(){
        return $this->belongsToMany('App\Book','book_to_autor');
    }
}

