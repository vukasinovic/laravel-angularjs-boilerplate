<?php


namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories_books extends Model
{
    //
    //protected $table='books';
    protected $primaryKey='id_category';
    //protected $dateFormat='U';
    public $timestamps = false;
    protected $fillable = [
        'name'
       
    ];
    
    public function book(){
        return $this->belongsToMany('App\Book','book_to_category');
    }
}
